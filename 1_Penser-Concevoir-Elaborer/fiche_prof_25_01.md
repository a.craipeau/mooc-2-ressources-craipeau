**Thématique :** Gestion des processus et des ressources par un système d’exploitation.

**Notions liées :** Etats d'un processus, ordonnancement de processus, interblocages

**Résumé de l’activité :** Reprise d'un exercice du bac réalisé par les élèves en binôme avec correction intermédiaire et retour sur les notions vues précédemment en cours.

**Objectifs :** 
Préparation des élèves aux exercices du bac :

- lecture de sujet
- découverte des questions posées dans un exercice de type bac, spécifiquement sur la partie "architecture matérielle, OS et réseaux" (ici, processus)

La tenue de corrections intermédiaires permet à chacun de réaliser l'exercice dans son intégralité sans blocage et d'avoir un exemple concret de sujet complété sur lequel s'appuyer lors de révisions futures

**Auteur :** Alexis Craipeau feat. les concepteurs de sujets de bac NSI 

**Durée de l’activité :** ~1 heure

**Forme de participation :** seul / en binôme

**Matériel nécessaire :** Aucun

**Préparation :** Cours préalable sur les processus et les notions d'ordonnancement et d'interblocage. TP sur ordinateur afin de présenter des commandes bash liées aux processus et illustrer le fonctionnement de ces derniers.

**Autres références :** Cours sur les processus